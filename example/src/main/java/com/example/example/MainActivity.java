package com.example.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

import cl.ceisufro.youtubeextractor.core.YouTubeExtractor;
import cl.ceisufro.youtubeextractor.core.YouTubeExtractorCallback;
import cl.ceisufro.youtubeextractor.model.YouTubeVideo;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String API_KEY = "";
        YouTubeExtractor youTubeExtractor = YouTubeExtractor.newBuilder(this, API_KEY)
                .addPlaylist("PLAYLIST_ID", 10)
                .build();
        youTubeExtractor.executeAsync(new YouTubeExtractorCallback() {
            @Override
            public void onTaskStarted() {
                Log.d("Example", "YoutubeExtractor#Start");
            }

            @Override
            public void onError(Throwable throwable) {
                Log.d("Example", "YoutubeExtractor#Error");
                throwable.printStackTrace();
            }

            @Override
            public void onTaskCompleted(ArrayList<YouTubeVideo> youTubeVideos) {
                Log.d("Example", "YoutubeExtractor#Finish");
                Log.d("Example", "Array Length = " + youTubeVideos.size());
                for (YouTubeVideo youTubeVideo : youTubeVideos) {
                    Log.d("Example", youTubeVideo.getVideoMeta().getTitle());
                }
            }
        });
    }
}
