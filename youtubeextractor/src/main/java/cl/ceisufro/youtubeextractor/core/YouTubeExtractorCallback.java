package cl.ceisufro.youtubeextractor.core;

import java.util.ArrayList;

import cl.ceisufro.youtubeextractor.model.YouTubeVideo;

/**
 * Creado por Luis Andrés Jara Castillo on 23-10-17.
 */

public interface YouTubeExtractorCallback {
    void onTaskStarted();

    void onError(Throwable throwable);

    void onTaskCompleted(ArrayList<YouTubeVideo> youTubeVideos);
}
