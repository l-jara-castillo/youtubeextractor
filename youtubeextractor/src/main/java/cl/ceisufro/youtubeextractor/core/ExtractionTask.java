package cl.ceisufro.youtubeextractor.core;

import android.content.Context;
import android.os.AsyncTask;
import android.util.SparseArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import at.huber.youtubeExtractor.YtFile;
import cl.ceisufro.youtubeextractor.model.VideoMeta;
import cl.ceisufro.youtubeextractor.model.YouTubeVideo;

class ExtractionTask {
    static ArrayList<YouTubeVideo> launchExtraction(YouTubeExtractor youTubeExtractor) throws IOException, InterruptedException, ExecutionException {
        ArrayList<YouTubeVideo> results = new ArrayList<>();
        if (!youTubeExtractor.getChannelIds().isEmpty()) {
            results.addAll(extractChannelsVideos(youTubeExtractor));
        }
        if (!youTubeExtractor.getPlaylistIds().isEmpty()) {
            results.addAll(extractPlaylistVideos(youTubeExtractor));
        }
        if (!youTubeExtractor.getVideoIds().isEmpty()) {
            results.addAll(extractVideos(youTubeExtractor));
        }
        return results;
    }

    static private ArrayList<YouTubeVideo> extractChannelsVideos(YouTubeExtractor youTubeExtractor) throws IOException, ExecutionException, InterruptedException {
        YouTubeRequester youTubeRequester = new YouTubeRequester(youTubeExtractor.getApiKey());
        ArrayList<YouTubeVideo> youTubeVideos = new ArrayList<>();
        for (String channelId : youTubeExtractor.getChannelIds().keySet()) {
            int maxResults = youTubeExtractor.getChannelIds().get(channelId);
            ArrayList<String> videoIds = new ArrayList<>(youTubeRequester.getChannelVideos(channelId, maxResults));
            for (String videoId : videoIds) {
                ExtractorResult extractorResult = new ExtractorWrapper(youTubeExtractor.getContext())
                        .launchExtraction(videoId);
                youTubeVideos.add(decodeExtractorResult(extractorResult));
            }
        }
        return youTubeVideos;
    }

    static private ArrayList<YouTubeVideo> extractPlaylistVideos(YouTubeExtractor youTubeExtractor) throws IOException, ExecutionException, InterruptedException {
        YouTubeRequester youTubeRequester = new YouTubeRequester(youTubeExtractor.getApiKey());
        ArrayList<YouTubeVideo> youTubeVideos = new ArrayList<>();
        for (String playlistId : youTubeExtractor.getPlaylistIds().keySet()) {
            int maxResults = youTubeExtractor.getPlaylistIds().get(playlistId);
            ArrayList<String> videoIds = new ArrayList<>(youTubeRequester.getPlayListVideos(playlistId, maxResults));
            for (String videoId : videoIds) {
                ExtractorResult extractorResult = new ExtractorWrapper(youTubeExtractor.getContext())
                        .launchExtraction(videoId);
                youTubeVideos.add(decodeExtractorResult(extractorResult));
            }
        }
        return youTubeVideos;
    }

    static private ArrayList<YouTubeVideo> extractVideos(YouTubeExtractor youTubeExtractor) throws ExecutionException, InterruptedException {
        ArrayList<YouTubeVideo> youTubeVideos = new ArrayList<>();
        for (String videoId : youTubeExtractor.getVideoIds()) {
            ExtractorResult extractorResult = new ExtractorWrapper(youTubeExtractor.getContext())
                    .launchExtraction(videoId);
            youTubeVideos.add(decodeExtractorResult(extractorResult));
        }
        return youTubeVideos;
    }

    static private YouTubeVideo decodeExtractorResult(ExtractorResult extractorResult) {
        YouTubeVideo video = null;
        if (extractorResult != null) {
            VideoMeta videoMeta = new VideoMeta(extractorResult.getVideoMeta().getVideoId(),
                    extractorResult.getVideoMeta().getTitle(),
                    extractorResult.getVideoMeta().getAuthor(),
                    extractorResult.getVideoMeta().getChannelId(),
                    extractorResult.getVideoMeta().getVideoLength(),
                    extractorResult.getVideoMeta().getViewCount(),
                    extractorResult.getVideoMeta().isLiveStream());
            video = new YouTubeVideo(videoMeta, extractorResult.getYtFiles());
        }
        return video;
    }

    void launchAsync(YouTubeExtractor youTubeExtractor, YouTubeExtractorCallback youTubeExtractorCallback) {
        new AsyncExtractor(youTubeExtractor, youTubeExtractorCallback).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    static class AsyncExtractor extends AsyncTask<Void, Void, ArrayList<YouTubeVideo>> {
        private YouTubeExtractorCallback youTubeExtractorCallback;
        private YouTubeExtractor youTubeExtractor;

        private AsyncExtractor(YouTubeExtractor youTubeExtractor, YouTubeExtractorCallback youTubeExtractorCallback) {
            this.youTubeExtractor = youTubeExtractor;
            this.youTubeExtractorCallback = youTubeExtractorCallback;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            youTubeExtractorCallback.onTaskStarted();
        }

        @Override
        protected ArrayList<YouTubeVideo> doInBackground(Void... params) {
            return getVideos();
        }

        @Override
        protected void onPostExecute(ArrayList<YouTubeVideo> youTubeVideos) {
            if (youTubeVideos != null)
                youTubeExtractorCallback.onTaskCompleted(youTubeVideos);
        }

        private ArrayList<YouTubeVideo> getVideos() {
            try {
                return launchExtraction(youTubeExtractor);
            } catch (Exception e) {
                youTubeExtractorCallback.onError(e);
                return null;
            }
        }
    }

    static class ExtractorWrapper extends at.huber.youtubeExtractor.YouTubeExtractor {
        private at.huber.youtubeExtractor.VideoMeta videoMeta;

        public ExtractorWrapper(Context con) {
            super(con);
        }


        @Override
        protected void onExtractionComplete(SparseArray<YtFile> ytFiles, at.huber.youtubeExtractor.VideoMeta videoMeta) {
            this.videoMeta = videoMeta;
        }


        ExtractorResult launchExtraction(String videoId) throws ExecutionException, InterruptedException {
            SparseArray<YtFile> ytFileSparseArray = this.execute(videoId).get();
            Thread.sleep(100);
            return new ExtractorResult(ytFileSparseArray, videoMeta);
        }
    }
}

