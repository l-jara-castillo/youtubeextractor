package cl.ceisufro.youtubeextractor.core;

import android.util.SparseArray;

import at.huber.youtubeExtractor.YtFile;

public class ExtractorResult {
    private SparseArray<YtFile> ytFiles;
    private at.huber.youtubeExtractor.VideoMeta videoMeta;

    public ExtractorResult(SparseArray<YtFile> ytFiles, at.huber.youtubeExtractor.VideoMeta videoMeta) {
        this.ytFiles = ytFiles;
        this.videoMeta = videoMeta;
    }

    public SparseArray<YtFile> getYtFiles() {
        return ytFiles;
    }

    public at.huber.youtubeExtractor.VideoMeta getVideoMeta() {
        return videoMeta;
    }
}