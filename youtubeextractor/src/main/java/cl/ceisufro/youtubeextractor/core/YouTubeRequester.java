package cl.ceisufro.youtubeextractor.core;

import android.text.TextUtils;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


class YouTubeRequester {
    private String API_KEY;
    private YouTube youtube;

    YouTubeRequester(String API_KEY) {
        this.API_KEY = API_KEY;
        buildYoutubeClient();
    }

    private void buildYoutubeClient() {
        youtube = new YouTube.Builder(new ApacheHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest request) throws IOException {
                request.setReadTimeout(10 * 1000);
                request.setConnectTimeout(10 * 1000);
            }
        }).build();
    }

    ArrayList<String> getChannelVideos(String channellId, int maxResults) throws IOException {
        if (maxResults <= 0) {
            SearchListResponse response = youtube.search().list("id")
                    .setKey(API_KEY)
                    .setChannelId(channellId)
                    .execute();
            maxResults = response.getPageInfo().getTotalResults();
        }
        ArrayList<SearchResult> videos = new ArrayList<>(searchChannelVideos(channellId, ""));
        Collections.sort(videos, new Comparator<SearchResult>() {
            @Override
            public int compare(SearchResult o1, SearchResult o2) {
                return Long.compare(o2.getSnippet().getPublishedAt().getValue(), o1.getSnippet().getPublishedAt().getValue());
            }
        });
        ArrayList<String> resultIds = new ArrayList<>();
        maxResults = videos.size() < maxResults ? videos.size() : maxResults;
        for (int i = 0; i < maxResults; i++) {
            SearchResult searchResult = videos.get(i);
            resultIds.add(searchResult.getId().getVideoId());
        }
        return resultIds;
    }

    private ArrayList<SearchResult> searchChannelVideos(String channelId, String pageToken) throws IOException {
        SearchListResponse response;
        YouTube.Search.List searchTask = youtube.search().list("snippet")
                .setKey(API_KEY)
                .setChannelId(channelId)
                .setOrder("date")
                .setType("video");
        if (!pageToken.equals("")) {
            searchTask.setPageToken(pageToken);
        }
        response = searchTask.execute();
        ArrayList<SearchResult> videos = new ArrayList<>(response.getItems());
        String nextPageToken = response.getNextPageToken();
        if (!TextUtils.isEmpty(nextPageToken)) {
            videos.addAll(searchChannelVideos(channelId, nextPageToken));
        }
        return videos;
    }

    ArrayList<String> getPlayListVideos(String playListId, int maxResults) throws IOException {
        ArrayList<String> videos;
        if (maxResults <= 0) {
            PlaylistItemListResponse response = youtube.playlistItems().list("status")
                    .setKey(API_KEY)
                    .setPlaylistId(playListId)
                    .execute();
            maxResults = response.getPageInfo().getTotalResults();
        }
        videos = new ArrayList<>();
        ArrayList<PlaylistItem> playlistItems = searchPlayListVideos(playListId, "");
        Collections.sort(playlistItems, new Comparator<PlaylistItem>() {
            @Override
            public int compare(PlaylistItem o1, PlaylistItem o2) {
                return Long.compare(o2.getSnippet().getPublishedAt().getValue(), o1.getSnippet().getPublishedAt().getValue());
            }
        });
        maxResults = playlistItems.size() < maxResults ? playlistItems.size() : maxResults;
        for (int i = 0; i < maxResults; i++) {
            videos.add(playlistItems.get(i).getSnippet().getResourceId().getVideoId());
        }
        return videos;
    }

    private ArrayList<PlaylistItem> searchPlayListVideos(String playlistId, String pageToken) throws IOException {
        PlaylistItemListResponse response;
        YouTube.PlaylistItems.List searchTask = youtube.playlistItems().list("snippet")
                .setKey(API_KEY)
                .setPlaylistId(playlistId)
                .setMaxResults(50L);
        if (!pageToken.equals("")) {
            searchTask.setPageToken(pageToken);
        }
        response = searchTask.execute();
        ArrayList<PlaylistItem> videos = new ArrayList<>(response.getItems());
        String nextPageToken = response.getNextPageToken();
        if (!TextUtils.isEmpty(nextPageToken)) {
            videos.addAll(searchPlayListVideos(playlistId, nextPageToken));
        }
        return videos;
    }
}
