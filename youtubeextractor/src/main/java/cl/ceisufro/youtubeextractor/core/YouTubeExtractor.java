package cl.ceisufro.youtubeextractor.core;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import cl.ceisufro.youtubeextractor.model.YouTubeVideo;

/**
 * Created by Luis Jara Castillo on 2017 for YoutubeDownloader.
 */

public class YouTubeExtractor {
    private Context context;
    private String API_KEY;
    private boolean includeWebM;
    private boolean useHttp;
    private boolean parseDashManifest;
    private boolean onlyIds;
    private HashMap<String, Integer> channelIds;
    private HashMap<String, Integer> playlistIds;
    private ArrayList<String> videoIds;

    private YouTubeExtractor(Builder builder) {
        this.context = builder.context;
        this.API_KEY = builder.API_KEY;
        this.channelIds = builder.channelIds;
        this.playlistIds = builder.playlistIds;
        this.videoIds = builder.videoIds;
        this.useHttp = builder.useHttp;
        this.includeWebM = builder.includeWebM;
        this.parseDashManifest = builder.parseDashManifest;
        this.onlyIds = builder.onlyIds;
    }

    public Context getContext() {
        return context;
    }

    public String getApiKey() {
        return API_KEY;
    }

    public boolean isIncludingWebM() {
        return includeWebM;
    }

    public boolean isUsingHttp() {
        return useHttp;
    }

    public boolean isParsingDashManifest() {
        return parseDashManifest;
    }

    public boolean isIdsOnlyMode() {
        return onlyIds;
    }

    public HashMap<String, Integer> getChannelIds() {
        return channelIds;
    }

    public HashMap<String, Integer> getPlaylistIds() {
        return playlistIds;
    }

    public ArrayList<String> getVideoIds() {
        return videoIds;
    }

    public static Builder newBuilder(Context context, String API_KEY) {
        return new Builder(context, API_KEY);
    }

    public void executeAsync(YouTubeExtractorCallback youTubeExtractorCallback) {
        new ExtractionTask().launchAsync(this, youTubeExtractorCallback);
    }

    public ArrayList<YouTubeVideo> execute() throws IOException, InterruptedException, ExecutionException {
        return new ExtractionTask().launchExtraction(this);
    }

    public static class Builder {
        private boolean includeWebM = true;
        private boolean useHttp = false;
        private boolean parseDashManifest = false;
        private boolean onlyIds = false;
        private Context context;
        private String API_KEY;
        private HashMap<String, Integer> channelIds = new HashMap<>();
        private HashMap<String, Integer> playlistIds = new HashMap<>();
        private ArrayList<String> videoIds = new ArrayList<>();

        public Builder(Context context, String API_KEY) {
            this.context = context;
            this.API_KEY = API_KEY;
        }

        public Builder setIncludeWebM(boolean include) {
            this.includeWebM = include;
            return this;
        }

        public Builder setUseHttp(boolean useHttp) {
            this.useHttp = useHttp;
            return this;
        }

        public Builder onlyReturnIds() {
            this.onlyIds = true;
            return this;
        }

        public Builder parseDashManifest(boolean parseDashManifest) {
            this.parseDashManifest = parseDashManifest;
            return this;
        }

        public Builder addChannel(String id, int maxVideos) {
            this.channelIds.put(id, maxVideos);
            return this;
        }

        public Builder addChannel(String id) {
            this.channelIds.put(id, 0);
            return this;
        }

        public Builder addPlaylist(String id, int maxVideos) {
            this.playlistIds.put(id, maxVideos);
            return this;
        }

        public Builder addPlaylist(String id) {
            this.playlistIds.put(id, 0);
            return this;
        }

        public Builder addVideo(String id) {
            this.videoIds.add(id);
            return this;
        }

        public Builder addVideoList(ArrayList<String> videoIds) {
            this.videoIds.addAll(videoIds);
            return this;
        }

        public YouTubeExtractor build() {
            return new YouTubeExtractor(this);
        }

        public ArrayList<YouTubeVideo> execute() throws IOException, InterruptedException, ExecutionException {
            return build().execute();
        }

        public YouTubeExtractor executeAsync(YouTubeExtractorCallback youTubeExtractorCallback) {
            YouTubeExtractor youTubeExtractor = build();
            youTubeExtractor.executeAsync(youTubeExtractorCallback);
            return youTubeExtractor;
        }
    }
}
