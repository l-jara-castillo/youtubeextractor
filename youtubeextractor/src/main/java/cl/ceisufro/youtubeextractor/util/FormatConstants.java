package cl.ceisufro.youtubeextractor.util;

/**
 * Creado por Luis Andrés Jara Castillo on 23-10-17.
 */

public class FormatConstants {
    // Video and Audio
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: MPEG4 <br>
     * Calidad: 144p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 24KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int THREEGP_144_VA = 17;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: MPEG4 <br>
     * Calidad: 240p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 32KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int THREEGP_240_VA = 36;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H263 <br>
     * Calidad: 240p <br>
     * Audio Codec: MP3 <br>
     * Bitrate: 64KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int FLV_240_VA = 5;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 (WebM) <br>
     * Video Codec: VP8 <br>
     * Calidad: 360p <br>
     * Audio Codec: VORBIS <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_360_VA = 43;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 (WebM) <br>
     * Video Codec: H264 <br>
     * Calidad: 360p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 96KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int MP4_360_VA = 18;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 (WebM) <br>
     * Video Codec: H264 <br>
     * Calidad: 720p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 192KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: NO <br>
     */
    public static int MP4_720_VA = 22;

    // Dash Video
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 144p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_144_DASH_VIDEO = 160;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 240p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_240_DASH_VIDEO = 133;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 360p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_360_DASH_VIDEO = 134;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 480p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_480_DASH_VIDEO = 135;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 720p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_720_DASH_VIDEO = 136;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 1080p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_1080_DASH_VIDEO = 137;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 1440p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_1440_DASH_VIDEO = 264;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 2160p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_2160_DASH_VIDEO = 266;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 720p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_720_60FPS_DASH_VIDEO = 298;
    /**
     * Archivo: VIDEO <br>
     * Formato: MP4 (DASH) <br>
     * Video Codec: H264 <br>
     * Calidad: 1080p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_1080_60FPS_DASH_VIDEO = 299;

    //Dash Audio
    /**
     * Archivo: AUDIO <br>
     * Formato: M4A (DASH) <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: AAC <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_128KBPS_DASH_AUDIO = 140;
    /**
     * Archivo: AUDIO <br>
     * Formato: M4A (DASH) <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: AAC <br>
     * Bitrate: 256KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int MP4_256KBPS_DASH_AUDIO = 141;

    // WEBM Dash Video
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 144p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_144_VIDEO = 278;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 240p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_240_VIDEO = 242;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 360p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_360_VIDEO = 243;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 480p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_480_VIDEO = 244;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 720p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_720_VIDEO = 247;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 1080p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_1080_VIDEO = 248;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 1440p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_1440_VIDEO = 271;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 2160p <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_2160_VIDEO = 313;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 720p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_720_60FPS_VIDEO = 302;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 1080p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_1080_60FPS_VIDEO = 303;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 1440p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_1440_60FPS_VIDEO = 308;
    /**
     * Archivo: VIDEO <br>
     * Formato: WEBM <br>
     * Video Codec: VP9 <br>
     * Calidad: 2160p 60FPS <br>
     * Audio Codec: NINGUNO <br>
     * Bitrate: NINGUNO <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_2160_60FPS_VIDEO = 315;

    // WEBM Dash Audio
    /**
     * Archivo: AUDIO <br>
     * Formato: WEBM <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: VORBIS <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_128KBPS_AUDIO = 171;
    /**
     * Archivo: AUDIO <br>
     * Formato: WEBM <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: OPUS <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_48KBPS_AUDIO = 249;
    /**
     * Archivo: AUDIO <br>
     * Formato: WEBM <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: OPUS <br>
     * Bitrate: 64KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_64KBPS_AUDIO = 250;
    /**
     * Archivo: AUDIO <br>
     * Formato: WEBM <br>
     * Video Codec: NINGUNO <br>
     * Calidad: NINGUNA <br>
     * Audio Codec: OPUS <br>
     * Bitrate: 160KBPS <br>
     * Dash Container: SI <br>
     * HLS Content: NO <br>
     */
    public static int WEBM_160KBPS_AUDIO = 251;

    // HLS Live Stream
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 144p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 48KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_144_LIVE_STREAM = 91;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 240p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 48KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_240_LIVE_STREAM = 92;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 360p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_360_LIVE_STREAM = 93;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 480p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 128KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_480_LIVE_STREAM = 94;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 720p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 256KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_720_LIVE_STREAM = 95;
    /**
     * Archivo: VIDEO & AUDIO <br>
     * Formato: MP4 <br>
     * Video Codec: H264 <br>
     * Calidad: 1080p <br>
     * Audio Codec: AAC <br>
     * Bitrate: 256KBPS <br>
     * Dash Container: NO <br>
     * HLS Content: SI <br>
     */
    public static int HLS_1080_LIVE_STREAM = 96;
}
