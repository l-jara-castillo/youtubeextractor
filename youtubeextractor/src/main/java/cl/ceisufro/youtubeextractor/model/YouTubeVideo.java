package cl.ceisufro.youtubeextractor.model;

import android.util.SparseArray;

import java.io.Serializable;
import java.util.Objects;

import at.huber.youtubeExtractor.YtFile;

public class YouTubeVideo implements Serializable {
    private VideoMeta videoMeta;
    private SparseArray<YtFile> extractedUrls;

    public YouTubeVideo(VideoMeta videoMeta, SparseArray<YtFile> extractedUrls) {
        this.videoMeta = videoMeta;
        this.extractedUrls = extractedUrls;
    }

    public VideoMeta getVideoMeta() {
        return videoMeta;
    }

    public SparseArray<YtFile> getExtractedUrls() {
        return extractedUrls;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof YouTubeVideo)) return false;
        YouTubeVideo that = (YouTubeVideo) o;
        return Objects.equals(getVideoMeta(), that.getVideoMeta()) &&
                Objects.equals(getExtractedUrls(), that.getExtractedUrls());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVideoMeta(), getExtractedUrls());
    }

    @Override
    public String toString() {
        return "YouTubeVideo{" +
                ", videoMeta=" + videoMeta +
                ", extractedUrls=" + extractedUrls +
                '}';
    }
}
