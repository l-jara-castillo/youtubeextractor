package cl.ceisufro.youtubeextractor.model;

import java.util.Objects;

/**
 * Created by Luis Jara Castillo on 2017 for android-youtubeExtractor-master.
 */

public class YouTubeChannel {
    private String channelId;
    private String channelName;

    public YouTubeChannel(String channelId, String channelName) {
        this.channelId = channelId;
        this.channelName = channelName;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YouTubeChannel that = (YouTubeChannel) o;
        return Objects.equals(getChannelId(), that.getChannelId()) &&
                Objects.equals(getChannelName(), that.getChannelName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getChannelId(), getChannelName());
    }
}
