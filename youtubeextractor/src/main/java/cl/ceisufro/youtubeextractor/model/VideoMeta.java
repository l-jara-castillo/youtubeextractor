package cl.ceisufro.youtubeextractor.model;

import java.util.Objects;

public class VideoMeta {
    private static final String IMAGE_BASE_URL = "http://i.ytimg.com/vi/";
    private String videoId;
    private String title;
    private String channelName;
    private String channelId;
    private long videoLength;
    private long viewCount;
    private boolean isLiveStream;

    public VideoMeta(String videoId, String title, String channelName, String channelId, long videoLength, long viewCount, boolean isLiveStream) {
        this.videoId = videoId;
        this.title = title;
        this.channelName = channelName;
        this.channelId = channelId;
        this.videoLength = videoLength;
        this.viewCount = viewCount;
        this.isLiveStream = isLiveStream;
    }

    // 120 x 90
    public String getThumbUrl() {
        return IMAGE_BASE_URL + videoId + "/default.jpg";
    }

    // 320 x 180
    public String getMqImageUrl() {
        return IMAGE_BASE_URL + videoId + "/mqdefault.jpg";
    }

    // 480 x 360
    public String getHqImageUrl() {
        return IMAGE_BASE_URL + videoId + "/hqdefault.jpg";
    }

    // 640 x 480
    public String getSdImageUrl() {
        return IMAGE_BASE_URL + videoId + "/sddefault.jpg";
    }

    // Max Res
    public String getMaxResImageUrl() {
        return IMAGE_BASE_URL + videoId + "/maxresdefault.jpg";
    }

    public String getVideoId() {
        return videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getChannelId() {
        return channelId;
    }

    public boolean isLiveStream() {
        return isLiveStream;
    }

    /**
     * The video length in seconds.
     */
    public long getVideoLength() {
        return videoLength;
    }

    public long getViewCount() {
        return viewCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VideoMeta)) return false;
        VideoMeta videoMeta = (VideoMeta) o;
        return getVideoLength() == videoMeta.getVideoLength() &&
                getViewCount() == videoMeta.getViewCount() &&
                isLiveStream() == videoMeta.isLiveStream() &&
                Objects.equals(getVideoId(), videoMeta.getVideoId()) &&
                Objects.equals(getTitle(), videoMeta.getTitle()) &&
                Objects.equals(getChannelName(), videoMeta.getChannelName()) &&
                Objects.equals(getChannelId(), videoMeta.getChannelId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVideoId(), getTitle(), getChannelName(), getChannelId(), getVideoLength(), getViewCount(), isLiveStream());
    }

    @Override
    public String toString() {
        return "VideoMeta{" +
                "videoId='" + videoId + '\'' +
                ", title='" + title + '\'' +
                ", channelName='" + channelName + '\'' +
                ", channelId='" + channelId + '\'' +
                ", videoLength=" + videoLength +
                ", viewCount=" + viewCount +
                ", isLiveStream=" + isLiveStream +
                '}';
    }
}
